# README #

###Bookstore ###
* This is a Bookstore Web Service that uses RESTful architecture.
* Allows users to GET a book, ADD a book, CREATE an order, UPDATE an order, GET an order and DELETE an order.
* Version 1.0


###--BOOK--###
* Accepts and returns json/xml
* Search book by title: (GET /bookservice/book/title/{bookTitle})
* Search book by author: (GET /bookservice/book/author/{bookauthor})
* Add book: (POST /bookservice/book)

###--ORDER--###
* Accepts and returns json/xml
* Search order by order number: (GET /orderservice/order/{orderNumber})
* Create an order: (POST /orderservice/order)
* Update an order: (PUT /orderservice/order)
* Delete an order: (DELETE /orderservice/order/{orderNumber})

###Client test included###

### Creators ###
* Audrey Redovan
* Madina Igibayeva