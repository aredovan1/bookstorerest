package edu.luc.cs.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Response;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import edu.luc.cs.bookstore.Book;
import edu.luc.cs.servicerepresentation.OrderRequest;
import edu.luc.cs.servicerepresentation.BookRequest;

public class BookOrderServiceClient {
	
	private BookOrderServiceClient() {
	} 

	public static void main(String args[]) throws Exception {

		List<Object> providers = new ArrayList<Object>();
		JacksonJsonProvider provider = new JacksonJsonProvider();
		provider.addUntouchable(Response.class);
		providers.add(provider);

		/*****************************************************************************************
		 * GET METHOD invoke
		 *****************************************************************************************/
		/*GET BOOK BY TITLE*/
		System.out.println("GET METHOD .........................................................");
		WebClient getClient = WebClient.create("http://localhost:8081", providers);
		WebClient getClient2 = WebClient.create("http://localhost:8081", providers);
		WebClient getClient3 = WebClient.create("http://localhost:8082", providers);

		//Configuring the CXF logging interceptor for the outgoing message
		WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
		//Configuring the CXF logging interceptor for the incoming response
		WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());

		// change application/xml  to get in xml format
		getClient = getClient.accept("application/xml").type("application/xml").path("/bookservice/book/title/Rant");

		//The following lines are to show how to log messages without the CXF interceptors
		String getRequestURI = getClient.getCurrentURI().toString();
		System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
		String getRequestHeaders = getClient.getHeaders().toString();
		System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);

		//to see as raw XML/json
		String response = getClient.get(String.class);
		System.out.println("GET METHOD Response: ...." + response +"\n");
		
		/*GET BOOK BY AUTHOR*/
		// change application/xml  to get in xml format
		getClient2 = getClient2.accept("application/json").type("application/json").path("/bookservice/book/author/Chuck-Palahniuk");

		//The following lines are to show how to log messages without the CXF interceptors
		getRequestURI = getClient2.getCurrentURI().toString();
		System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
		getRequestHeaders = getClient2.getHeaders().toString();
		System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);

		//to see as raw XML/json
		String response2 = getClient2.get(String.class);
		System.out.println("GET METHOD Response: ...." + response2 +"\n");
		
		/*GET ORDER BY ORDERNUMBER*/
		// change application/xml  to get in xml format
		getClient3 = getClient3.accept("application/json").type("application/json").path("/orderservice/order/#1111");

		//The following lines are to show how to log messages without the CXF interceptors
		getRequestURI = getClient3.getCurrentURI().toString();
		System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
		getRequestHeaders = getClient3.getHeaders().toString();
		System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);

		//to see as raw XML/json
		String response3 = getClient3.get(String.class);
		System.out.println("GET METHOD Response: ...." + response3 +"\n");

		/*****************************************************************************************
		 * POST METHOD invoke
		 *****************************************************************************************/
		/*ADD BOOK*/
		System.out.println("POST METHOD .........................................................");
		WebClient postClient = WebClient.create("http://localhost:8081", providers);
		WebClient postClient2 = WebClient.create("http://localhost:8082", providers);
		WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
		WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());

		// change application/xml  to application/json get in json format
		postClient = postClient.accept("application/xml").type("application/json").path("/bookservice/book");

		String postRequestURI = postClient.getCurrentURI().toString();
		System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
		String postRequestHeaders = postClient.getHeaders().toString();
		System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
		
		BookRequest bookRequest = new BookRequest();
		bookRequest.setBookAuthor("Chuck-Palahnuik");
		bookRequest.setBookTitle("Fight-Club");
		bookRequest.setBookPrice("$17.00");
		
		String responsePost =  postClient.post(bookRequest, String.class);
		System.out.println("POST METHOD Response ........." + responsePost + "\n");
		
		/*CREATE ORDER*/
		// change application/xml  to application/json get in json format
		postClient2 = postClient2.accept("application/json").type("application/xml").path("/orderservice/order");

		postRequestURI = postClient2.getCurrentURI().toString();
		System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
		postRequestHeaders = postClient2.getHeaders().toString();
		System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
		OrderRequest orderRequest = new OrderRequest();
		Book book = new Book();
		book.setBookTitle("Rant");
		book.setBookAuthor("Chuck-Palahniuk");
		book.setBookPrice("$15.00");
		orderRequest.setBookOrderInfo(book);
		orderRequest.setShippingInfo("1235-Something-St.");
		orderRequest.setPaymentInfo("23947230974");

		String responsePost2 =  postClient2.post(orderRequest, String.class);
		System.out.println("POST METHOD Response ........." + responsePost2 + "\n");
		
		/*****************************************************************************************
		 * DELETE METHOD invoke
		 *****************************************************************************************/
		System.out.println("DELETE METHOD .........................................................");
		WebClient deleteClient = WebClient.create("http://localhost:8082", providers);
		WebClient.getConfig(deleteClient).getOutInterceptors().add(new LoggingOutInterceptor());
		WebClient.getConfig(deleteClient).getInInterceptors().add(new LoggingInInterceptor());

		// change application/xml  to application/json get in json format
		deleteClient = deleteClient.accept("application/json").type("application/xml").path("/orderservice/order/#1111");

		String deleteRequestURI = deleteClient.getCurrentURI().toString();
		System.out.println("Client DELETE METHOD Request URI:  " + deleteRequestURI);
		String deleteRequestHeaders = deleteClient.getHeaders().toString();
		System.out.println("Client DELETE METHOD Request Headers:  " + deleteRequestHeaders);

		deleteClient.delete();
		System.out.println("DELETE METHOD Response ......... OK");

		System.exit(0);
	}
}
