package edu.luc.cs.bookstore;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Book implements Serializable {

	private static final long serialVersionUID = 1L;
	private String bookId;
	private String bookTitle;
	private String bookAuthor;
	private String bookPrice;
	
	public Book(){}

	public String getBookId() {
		return bookId;
	}

	public void setBookId(String id) {
		this.bookId = id;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookAuthor() {
		return bookAuthor;
	}

	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}
	
	public String getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(String bookPrice) {
		this.bookPrice = bookPrice;
	}


}