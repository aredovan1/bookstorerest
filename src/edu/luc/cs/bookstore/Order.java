package edu.luc.cs.bookstore;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Order implements Serializable{

	private static final long serialVersionUID = 1L;
	String paymentInfo;
	String shippingInfo;
	String orderNum = "";
	String orderStatus;
	Book book;
	
	public Order(){}

	public String getPaymentInfo() {
		return paymentInfo;
	}
	public void setPaymentInfo(String paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
	public String getShippingInfo() {
		return shippingInfo;
	}
	public void setShippingInfo(String shippingInfo) {
		this.shippingInfo = shippingInfo;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public void setBookOrderInfo(Book book) {
		this.book = book;
	}
	public Book getBookOrderInfo() {
		return book;
	}
	
}
