package edu.luc.cs.bookstore;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class CompositeDAO {

	private static Set<Book> books = new HashSet<Book>();
	private static Set<Order> orders = new HashSet<Order>();
	
	public CompositeDAO(){
		Book book = new Book();
		
		book.setBookId("#0000");
		book.setBookAuthor("Chuck-Palahniuk");
		book.setBookTitle("Rant");
		book.setBookPrice("$15.00");
		
		books.add(book);
		
		Order order = new Order();
		
		order.setBookOrderInfo(book);
		order.setOrderNum("#1111");
		order.setOrderStatus("Processing...");
		order.setPaymentInfo("7261839");
		order.setShippingInfo("Moon");
		
		orders.add(order);
	}
	
	public Book getBookByTitle(String bookTitle) {
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookTitle().equals(bookTitle)) {
				return book;
			}
		}
		return null;
	}
	
	public Book getBookByAuthor(String bookAuthor) {
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookAuthor().equals(bookAuthor)) {
				return book;
			}
		}
		return null;
	}
	
	public Book addBook(String bookTitle, String bookAuthor, String bookPrice) {
		Book book = new Book();
		
		Random random = new Random();
		int randomInt = random.nextInt(10000);
		String id = "#"+randomInt;
		
		book.setBookId(id);
		book.setBookAuthor(bookAuthor);
		book.setBookTitle(bookTitle);
		book.setBookPrice(bookPrice);
		
		books.add(book);
		
		return book;
	}
	
	public Order createOrder(String bookTitle, String shippingInfo, String paymentInfo) {
		Order order = new Order();
		Iterator<Book> it = books.iterator();
		while(it.hasNext()) {
			Book book = (Book)it.next();
			if(book.getBookTitle().equals(bookTitle)) {
				
				Random random = new Random();
				int randomInt = random.nextInt(10000);
				String orderNum = "#"+randomInt;
				
				order.setOrderNum(orderNum);
				order.setOrderStatus("PROCESSING");
				order.setPaymentInfo(paymentInfo);
				order.setShippingInfo(shippingInfo);
				order.setBookOrderInfo(book);
				orders.add(order);
				return order;
			}
		}
		return null;
	}
	
	public Order getOrder(String orderNumber) {
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = (Order)it.next();
			if (order.getOrderNum().equals(orderNumber)) {
				return order;
			}
		}
		return null;
	}
	
	public void updateOrder(String orderNum, String paymentInfo) {
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = (Order)it.next();
			if(order.getOrderNum().equals(orderNum)) {
				order.setPaymentInfo(paymentInfo);
				order.setOrderStatus("SHIPPED");
				return;
			}
		}
	}
	
	public void deleteOrder(String orderNum) {
		Iterator<Order> it = orders.iterator();
		while(it.hasNext()) {
			Order order = (Order)it.next();
			if(order.getOrderNum().equals(orderNum)) {
				orders.remove(order);
				return;
			}
		}
	}
	
}
