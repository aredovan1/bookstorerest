package edu.luc.cs.service;

import javax.jws.WebService;

import edu.luc.cs.servicerepresentation.BookRepresentation;
import edu.luc.cs.servicerepresentation.BookRequest;

@WebService
public interface BookService {
	
	public BookRepresentation addBook(BookRequest bookRequest);
	public BookRepresentation getBookByTitle(String bookTitle);
	public BookRepresentation getBookByAuthor(String bookAuthor);

}
