package edu.luc.cs.service;

import javax.jws.WebService;
import javax.ws.rs.core.Response;

import edu.luc.cs.servicerepresentation.OrderRepresentation;
import edu.luc.cs.servicerepresentation.OrderRequest;

@WebService
public interface OrderService {
	
	public OrderRepresentation getOrder(String orderNumber);
	public OrderRepresentation createOrder(OrderRequest orderRequest);
	
	public Response updateOrder(OrderRequest orderRequest);
	public Response deleteOrder(String orderNumber);

}
