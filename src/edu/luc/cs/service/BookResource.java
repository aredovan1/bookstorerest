package edu.luc.cs.service;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import edu.luc.cs.servicerepresentation.BookRepresentation;
import edu.luc.cs.servicerepresentation.BookRequest;
import edu.luc.cs.serviceworkflow.BookActivity;

@Path("/bookservice/")
public class BookResource implements BookService{

	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/book")
	public BookRepresentation addBook(BookRequest bookRequest) {
		System.out.println("POST METHOD Request from Client with ............." 
				+ bookRequest.getBookTitle() + " by: " + bookRequest.getBookAuthor());
		BookActivity bookActivity = new BookActivity();
		return bookActivity.addBook(bookRequest.getBookTitle(), bookRequest.getBookAuthor(), bookRequest.getBookPrice());
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/book/title/{bookTitle}")
	public BookRepresentation getBookByTitle(@PathParam("bookTitle") String title) {
		System.out.println("GET METHOD Request from Client with bookRequest String ............." + title);
		BookActivity bookActivity = new BookActivity();
		return bookActivity.getBookByTitle(title);
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/book/author/{bookAuthor}")
	public BookRepresentation getBookByAuthor(@PathParam("bookAuthor") String author) {
		System.out.println("GET METHOD Request from Client with bookRequest String ............." + author);
		BookActivity bookActivity = new BookActivity();
		return bookActivity.getBookByAuthor(author);
	}

}
