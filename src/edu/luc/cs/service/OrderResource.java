package edu.luc.cs.service;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.luc.cs.servicerepresentation.OrderRepresentation;
import edu.luc.cs.servicerepresentation.OrderRequest;
import edu.luc.cs.serviceworkflow.OrderActivity;

@Path("/orderservice/")
public class OrderResource implements OrderService{

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/order/{orderNumber}")
	public OrderRepresentation getOrder(@PathParam("orderNumber") String number) {
		System.out.println("GET METHOD Request from Client with orderRequest String ............." + number);
		OrderActivity orderActivity = new OrderActivity();
		return orderActivity.getOrder(number);
	}

	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/order")
	public OrderRepresentation createOrder(OrderRequest orderRequest) {
		System.out.println("POST METHOD Request from Client with ............." + orderRequest.getBookOrderInfo().getBookTitle() 
				+ " by: " + orderRequest.getShippingInfo());
		OrderActivity orderActivity = new OrderActivity();
		return orderActivity.createOrder(orderRequest.getBookOrderInfo().getBookTitle(), orderRequest.getShippingInfo(), orderRequest.getPaymentInfo());
	}

	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/order")
	public Response updateOrder(OrderRequest orderRequest) {
		System.out.println("UPDATE METHOD Request from Client with ............." + orderRequest.getOrderNum());
		OrderActivity orderActivity = new OrderActivity();
		String res = orderActivity.updateOrder(orderRequest.getOrderNum(), orderRequest.getPaymentInfo());
		
		if (res.equals("STATUS UPDATED")) {
			return Response.status(Status.OK).build();
		}
		
		return null;
	}

	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/order/{orderNumber}")
	public Response deleteOrder(@PathParam("orderNumber") String number) {
		System.out.println("Delete METHOD Request from Client with orderRequest String ............." + number);
		OrderActivity orderActivity = new OrderActivity();
		String res = orderActivity.deleteOrder(number);
		
		if (res.equals("ORDER CANCELLED")) {
			return Response.status(Status.OK).build();
		}
		
		return null;
	}

}
