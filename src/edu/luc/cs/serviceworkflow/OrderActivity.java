package edu.luc.cs.serviceworkflow;

import edu.luc.cs.bookstore.CompositeDAO;
import edu.luc.cs.bookstore.Order;
import edu.luc.cs.servicerepresentation.OrderRepresentation;

public class OrderActivity {
	
	CompositeDAO dao = new CompositeDAO();
	
	public OrderRepresentation createOrder(String bookTitle, String shippingInfo, String paymentInfo) {
		Order order = dao.createOrder(bookTitle, shippingInfo, paymentInfo);
		
		OrderRepresentation orderRep = new OrderRepresentation();
		orderRep.setBookOrderInfo(order.getBookOrderInfo());
		orderRep.setOrderNum(order.getOrderNum());
		orderRep.setOrderStatus(order.getOrderStatus());
		orderRep.setShippingInfo(shippingInfo);
		orderRep.setPaymentInfo(paymentInfo);
		
		return orderRep;
	}
	
	public OrderRepresentation getOrder(String orderNumber) {
		Order order = dao.getOrder(orderNumber);
		
		OrderRepresentation orderRep = new OrderRepresentation();
		orderRep.setBookOrderInfo(order.getBookOrderInfo());
		orderRep.setOrderNum(orderNumber);
		orderRep.setOrderStatus(order.getOrderStatus());
		orderRep.setShippingInfo(order.getShippingInfo());
		orderRep.setPaymentInfo(order.getPaymentInfo());
		
		return orderRep;
	}
	
	public String deleteOrder(String orderNumber) {
		
		dao.deleteOrder(orderNumber);
		
		return "ORDER CANCELLED";
	}
	
	public String updateOrder(String orderNumber, String paymentInfo) {
		
		dao.updateOrder(orderNumber, paymentInfo);
		
		return "STATUS UPDATED";
		
	}

}
