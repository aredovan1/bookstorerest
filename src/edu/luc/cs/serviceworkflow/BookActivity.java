package edu.luc.cs.serviceworkflow;

import edu.luc.cs.bookstore.Book;
import edu.luc.cs.bookstore.CompositeDAO;
import edu.luc.cs.servicerepresentation.BookRepresentation;

public class BookActivity {
	
	CompositeDAO dao = new CompositeDAO();
	
	public BookRepresentation getBookByTitle(String bookTitle) {
		
		Book book = dao.getBookByTitle(bookTitle);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		return bookRep;
	}
	
	public BookRepresentation getBookByAuthor(String bookAuthor) {
		
		Book book = dao.getBookByAuthor(bookAuthor);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		return bookRep;
	}
	
	public BookRepresentation addBook(String bookTitle, String bookAuthor, String bookPrice) {
		
		Book book = dao.addBook(bookTitle, bookAuthor, bookPrice);
		BookRepresentation bookRep = new BookRepresentation();
		bookRep.setBookTitle(book.getBookTitle());
		bookRep.setBookAuthor(book.getBookAuthor());
		bookRep.setBookId(book.getBookId());
		bookRep.setBookPrice(book.getBookPrice());
		
		return bookRep;
	}
	
	
}
